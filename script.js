function carregar() {
    var msg = window.document.getElementById('msg')
    var img = window.document.getElementById('imagem')
    var data = new Date()
    var hora = data.getHours()
    if (hora >= 0 && hora < 12) {
        msg.style.color = 'yellow'
        msg.innerHTML = `Agora são ${hora} horas.`
        msg.innerHTML += `<p>Bom dia!</p>`
        img.src = "fotodia.png"
        document.body.style.background = 'yellow'
    } else if (hora >= 12 && hora < 18) {
        msg.style.color = 'orange'
        msg.innerHTML = `Agora são ${hora} horas.`
        msg.innerHTML += `<p>Boa tarde!</p>`
        img.src = "fototarde.png"
        document.body.style.background = 'orange'
    } else {
        msg.style.color = 'blue'
        msg.innerHTML = `Agora são ${hora} horas.`
        msg.innerHTML += `<p>Boa noite!</p>`
        img.src = "fotonoite.png"
        document.body.style.background = 'blue'
    }
}